"""Mobile demo. This demo will allocate all buses, installing the
UHD tools, GNU radio, and X11 VNC to run uhd_fft. 

Instructions:
Wait for the experiment to start and for the __execute__ services to
finish (watch for the *checkmark* in the upper right of the node icons
in the topology diagram). Once you see the checkmark, click on a node
to get the context menu for that node, and then click on the VNC option.

The first time you do this, your browser might prompt you to allow
popup windows, be sure to allow that.

**If you want to access the radio directly,**
***you must terminate uhd_fft***. Open the VNC window or a shell window 
and type `/local/repository/kill-fft.csh`

You can get streaming GPSD data from the local LTE modem using the
`gpspipe` command:

```
        gpspipe -w control
```

The data can be streamed from any location if you use a fully qualified
name for the `-w` option (`control.bus-XXXX.powderwireless.net`). Consult
the man page for more information. Note that there is no PPS information,
so you cannot use it for high fidelity synchronization.

More details (with pictures): https://gitlab.flux.utah.edu/powder-profiles/mobile-demo

"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Emulab specific extensions.
import geni.rspec.emulab as emulab
# Route specific extensions.
import geni.rspec.emulab.route as route

# This is the current default image on buses.
OSIMAGE = 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD'

#
# Install script from the repository. This installs the uhd tools,
# gnuradio, and starts up uhd_fft (displayed in the X11 VNC console).
#
INSTALL = 'sudo /bin/bash /local/repository/install.sh'

# Create a Request object to start building the RSpec.
request = portal.context.makeRequestRSpec()

#
# Declare that you will be starting X11 VNC on (some of) your nodes.
# You must have this line for X11 VNC to work.
#
request.initVNC(
 
#
# Request all bus routes. On a typical day, 4-6 buses are running across
# a few common routes. Morning is when the most buses run.
#
allroutes = request.requestAllRoutes()
allroutes.disk_image = OSIMAGE
allroutes.addService(pg.Execute(shell="sh", command=INSTALL))
allroutes.startVNC()

portal.context.printRequestRSpec()
