This profile is a simple demonstration of how to allocate and use mobile endpoints (buses). 
All of the buses will be allocated and the UHD tools and GNU Radio will be installed, and
X11 VNC started. Lastly, uhd_fft will automatically start and run. 

To access the X11 VNC window, wait for the __execute__ services to
finish (watch for the *checkmark* in the upper right of the node icons
in the topology diagram). Once you see the checkmark, click on a node
to get the context menu for that node, and then click on the VNC option:

![alt text](images/menu.png "Context Menu")

A new window will be popped up. The first time you do this, your browser might prompt you to allow
popup windows, be sure to allow that. 

![alt text](images/uhd_fft.png "UHD FFT")

You can get streaming GPSD data from the local LTE modem using the
`gpspipe` command:

```
        gpspipe -w control
```

The data can be streamed from any location if you use a fully qualified
name for the `-w` option (`control.bus-XXXX.powderwireless.net`). Consult
the man page for more information. Note that there is no PPS information,
so you cannot use it for high fidelity synchronization.
